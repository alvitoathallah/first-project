from django.shortcuts import render, redirect
from .models import inputan as tabel
from .forms import inputanForm

# Create your views here.
def landpage(request):
    return render(request, 'home/landpage.html')
def about(request):
    return render(request, 'home/about.html')
def study(request):
    return render(request, 'home/study.html')
def socmed(request):
    return render(request, 'home/socmed.html')

def Join(request):
    form = inputanForm(request.POST)
    if request.method == "POST": 
        if form.is_valid():
            inp = tabel()
            inp.Mata_Kuliah = form.cleaned_data['Mata_Kuliah']
            inp.Dosen = form.cleaned_data['Dosen']
            inp.SKS = form.cleaned_data['SKS']
            inp.Semester = form.cleaned_data['Semester']
            inp.Kelas = form.cleaned_data['Kelas']
            inp.Deskripsi = form.cleaned_data['Deskripsi']
            inp.save()
            return redirect('/inputan')

    inp = tabel.objects.all()
    form = inputanForm()
    response = {"inp":inp, 'form' : form}
    return render(request,'home/inputan.html',response)

def inp_delete(request,pk):
    form = inputanForm(request.POST)
    if request.method == "POST":
        
        if form.is_valid():
            inp = tabel()
            inp.Mata_Kuliah = form.cleaned_data['Mata_Kuliah']
            inp.Dosen = form.cleaned_data['Dosen']
            inp.SKS = form.cleaned_data['SKS']
            inp.Semester = form.cleaned_data['Semester']
            inp.Kelas = form.cleaned_data['Kelas']
            inp.Deskripsi = form.cleaned_data['Deskripsi']
            inp.save()
            return redirect('/inputan')
    
    tabel.objects.filter(pk=pk).delete()
    data = tabel.objects.all()
    form = inputanForm()
    response = {"inp":data,'form':form}
    return redirect('/inputan')