from django.db import models

# Create your models here.
class inputan(models.Model):
    Mata_Kuliah = models.CharField(max_length = 50)
    Dosen = models.CharField(max_length = 50)
    SKS = models.CharField(max_length = 3)
    Semester = models.CharField(max_length = 50)
    Kelas = models.CharField(max_length = 50)
    Deskripsi = models.CharField(max_length = 100, default = "-")
    