from django import forms
from .models import inputan
from django.forms import ModelForm

class inputanForm(forms.ModelForm):
    class Meta:
        model = inputan
        fields = [
            'Mata_Kuliah',
            'Dosen',
            'SKS',
            'Semester',
            'Kelas',
            'Deskripsi',
        ]

        widgets = {
            'Mata_Kuliah' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'ex Fisika Dasar',
                    'required' : True,
                }
            ),
            'Dosen' : forms.TextInput(
                attrs ={
                    'class' : 'form-control',
                    'placeholder' : 'Nama dosen',
                    'required' : True,
                }
            ),
            'SKS' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Jumlah SKS',
                    'required' : True,
                }
            ),
            'Semester' : forms.TextInput(
                attrs ={
                    'class' : 'form-control',
                    'placeholder' : 'Tahun ajaran',
                    'required' : True,
                }
            ),
            'Kelas' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'ex 2.2401',
                    'required' : True,
                }
            ),
            'Deskripsi' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'Deskripsi mata kuliah',
                    'required' : True,
                }
            )
        }


    
