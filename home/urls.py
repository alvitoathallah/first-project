from django.urls import path
from .views import Join, inp_delete
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.landpage, name='landpage'),
    path('about', views.about, name='about'),
    path('study', views.study, name='study'),
    path('socmed', views.socmed, name='socmed'),
    path('inputan', views.Join, name='inputan'),
    path('<int:pk>', inp_delete, name='Delete'),
]