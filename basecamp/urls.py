from django.urls import path

from . import views

app_name = 'basecamp'

urlpatterns = [
    path('story1', views.index, name='Story1'),
]